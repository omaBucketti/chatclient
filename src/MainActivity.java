package com.example.komol.chatclient;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.PrintWriter;
import java.net.Socket;

public class MainActivity extends AppCompatActivity {

    // Declare the views
    public TextView messageLog;
    public EditText input;
    public Button send;

    // Global constants - YOU MIGHT NEED TO CHANGE THE IP ADDRESS TO MATCH THE SERVER!
    private final String ipServer = "192.168.0.100";
    private final int portNumServer = 1024;
    private Socket s = new Socket();

    // The handler
    public Handler io;

    // The preferences object
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Init the views
        messageLog = (TextView) findViewById(R.id.messageLog);
        input = (EditText) findViewById(R.id.input);

        // Create the handler
        io = new Handler() {
            @Override
            public void handleMessage(Message message) {
                String m = message.getData().getString("message");
                appendTextToLog(m);
            }
        };

        // Create the input thread
        Thread connectionIn = new Thread(new ChatIn(s, io, ipServer, portNumServer));
        connectionIn.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
          if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

    }


    ///////////////////////////////////////////////////////////////////////
    // UI METHODS
    ///////////////////////////////////////////////////////////////////////

    public void onSend(View b) {
        // Get the message from user
        String message = input.getText().toString();
        // Send the message
        SendMessageAsync send = new SendMessageAsync();
        send.execute(new SendMeta(s, message));
    }

    public void appendTextToLog(String m) {
        String currentText = messageLog.getText().toString();
        messageLog.setText(currentText + m + "\n");
    }

    // Private class to send message to the server
    private class SendMessageAsync extends AsyncTask<SendMeta, Void, String> { // params, progress, result

        @Override
        protected String doInBackground(SendMeta... params) {

            SendMeta meta = params[0];

            try {
                if (meta.s.isConnected()) {
                    // Create the output stream
                    PrintWriter out = new PrintWriter(meta.s.getOutputStream(), true);
                    // Write the data to the socket
                    out.println(meta.message);
                } else {
                    System.out.println("Socket is not connected");
                    return null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

            // Message was sent successfully
            return meta.message;
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) { // Message was sent, so...
                // Put the message into the log locally
                appendTextToLog(result);
                // Reset the TextView
                input.setText("");
            } else { // It failed and we don't really know why
                System.out.print("Message failed to send");
            }
        }
    }
}