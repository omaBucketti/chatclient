package com.example.komol.chatclient;

import java.net.Socket;

// Used as  a data wrapper for sending a message
public class SendMeta {

    public Socket s;
    public String message;

    public SendMeta(Socket s, String message) {
        this.s = s;
        this.message = message;
    }
}
