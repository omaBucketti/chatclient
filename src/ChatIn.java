package com.example.komol.chatclient;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;

public class ChatIn implements Runnable {

    private Socket s;
    private String ipServer;
    private int portNumServer;
    private Handler io;

    // Constructor
    public ChatIn(Socket s, Handler io, String ipServer, int portNumServer) {
        this.s = s;
        this.ipServer = ipServer;
        this.portNumServer = portNumServer;
        this.io = io;
    }

    private void receiveMessage(String msg) {
        Message msgObj = io.obtainMessage();
        Bundle bundle = new Bundle();
        bundle.putString("message", msg);
        msgObj.setData(bundle);
        io.sendMessage(msgObj);
    }

    @Override
    public void run() {
        try {
            // Connect the socket
            s.connect(new InetSocketAddress(ipServer, portNumServer));
            // Create the input stream
            BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
            // Keep listening on the socket
            while (true) {

                String m;
                if ((m = in.readLine()) != null) {
                    receiveMessage(m);
                }

            }
        } catch (Exception e) {
            System.out.println("IO Error");
            e.printStackTrace();
        }
    }
}
